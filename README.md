# Java Code Sample Application

This is a Sample Java Spring Boot (Maven) application written for ATARC use.  

## The pattern of usage

This code has been developed to assist with basic DevSecOps implementation and visualization.  The repository stands as is to be used with any CI/CD tool.  Jenkins can use the code to apply DevSecOps patterns defined in the [ATARC CI/CD Project](https://gitlab.com/atarc/cicd-project).  GitLab can use it with the provided .gitlab-ci.yml file which inherently uses GitLab's Auto DevOps features to identify, build and test code automatically by inclusing specific pattern ymls that are predefined and included with the GitLab installation. 

## Stages addressed

The included .gitlab-ci.yml file accomplishes the implementation of the following stages. Please see pipeline run to see the more details.

| Stage | Completion Status | Notes |
| ------ | ------ | ------ | 
| Stage: DevSecOps.CICD.Source-Pull | :white_check_mark: | GitLab inherently pulls in the code by performing a git clone using the GitLab runner operation. No additional operation needed unless there is a need to pull code from a different repository. |
| Stage: DevSecOps.CICD.Build | :white_check_mark:  | Performs a build using herokuish build pack to identify the language.  In the case of this project, the pack is for maven/Java |
| Stage: DevSecOps.CICD.Code-Coverage | [![coverage report](https://gitlab.com/atarc/java-sample-app/badges/master/coverage.svg)](https://gitlab.com/atarc/java-sample-app/-/commits/master) | 
| Stage: DevSecOps.CICD.Quality-Scan | :white_check_mark: | GitLab initializes the quality scan performed by CodeClimate which uses open source components to perform the scan and deliver the report to GitLab |
| Stage: DevSecOps.CICD.Security-Scan | :white_check_mark: | GitLab initializes and runs SAST scan on the code to provide visibility into any CVEs or CWEs in the code. |
| Stage: DevSecOps.CICD.Push-to-Artifact-Repo | :white_check_mark: | GitLab creates a container with the running app and pushes it to the built in Container Registry for further use. GitLab also pushes the Maven package into the GitLab package registry for use for distribution and deployment.|
| Stage: DevSecOps.CICD.Pull-from-artifact-Repo | :white_check_mark: | GitLab uses the container pushed to the Container registry to pull and deploy to destination. GitLab also pushes the Maven package into the GitLab package registry for use for distribution and deployment. |
| Stage: DevSecOps.CICD.Deploy | :white_check_mark: | If using Kubernetes, GitLab can be directly tied to the K8s environment and perform deployment.  If using traditional VM or bare metal, GitLab yml can be configured to perform the steps.  Please review the work in the [IaC Terraform Project](https://gitlab.com/atarc/iac-terraform)|
| Stage: DevSecOps.CICD.Smoke-Test | | Smoke tests can be written and executed via Selenium or other tools. |
| Stage: DevSecOps.CICD.Monitoring | | If using Kubernetes, GitLab can monitor the environment and collect all the metrics and incident information to initiate appropriate workflow.  If using traditional VM or bare metal, GitLab can integrate with various tools like Splunk or similar aggregating and monitoring tools. |

The End
