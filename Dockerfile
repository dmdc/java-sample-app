FROM openjdk:13-alpine
VOLUME /tmp
ADD /target/*.jar java-sample-app.jar
ENTRYPOINT ["java","-jar","/java-sample-app-0.0.1-SNAPSHOT.jar"]